Array.prototype.remove = function() {
	var what, a = arguments, L = a.length, ax;
	while (L && this.length) {
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1) {
			this.splice(ax, 1);
		}
	}
	return this;
};

function listToMatrix(list, elementsPerSubArray) {
	var matrix = [], i, k;
	for (i = 0, k = -1; i < list.length; i++) {
		if (i % elementsPerSubArray === 0) {
			k++;
			matrix[k] = [];
		}
		matrix[k].push(list[i]);
	}
	// for (i = 0; i < elementsPerSubArray-(list.length%elementsPerSubArray); i++)
	// 	matrix[k].push("");
	return matrix;
}

var region = [];
var continent = [];
var institution = [];
var result = [];
var result_institution = [];
var names = [];

function translateNameToDBLP(name) {
	// Ex: "Emery D. Berger" -> "http://dblp.uni-trier.de/pers/hd/b/Berger:Emery_D="
	// First, replace spaces and non-ASCII characters (not complete).
	// Known issue: does not properly handle suffixes like Jr., III, etc.
	name = name.replace(/'|\-|\./g, "=");
	//	name = name.replace(/\-/g, "=");
	//	name = name.replace(/\./g, "=");
	name = name.replace(/Á/g, "=Aacute=");
	name = name.replace(/á/g, "=aacute=");
	name = name.replace(/è/g, "=egrave=");
	name = name.replace(/é/g, "=eacute=");
	name = name.replace(/ï/g, "=iuml=");
	name = name.replace(/ó/g, "=oacute=");
	name = name.replace(/ç/g, "=ccedil=");
	name = name.replace(/ä/g, "=auml=");
	name = name.replace(/ö/g, "=ouml=");
	name = name.replace(/ø/g, "=oslash=");
	name = name.replace(/Ö/g, "=Ouml=");
	name = name.replace(/ü/g, "=uuml=");
	var splitName = name.split(" ");
	var lastName = splitName[splitName.length - 1];
	var disambiguation = "";
	if (parseInt(lastName) > 0) {
		// this was a disambiguation entry; go back.
		disambiguation = lastName;
		splitName.pop();
		lastName = splitName[splitName.length - 1] + "_" + disambiguation;
	}
	splitName.pop();
	var newName = splitName.join(" ");
	newName = newName.replace(/\s/g, "_");
	newName = newName.replace(/\-/g, "=");
	var str = "http://dblp.uni-trier.de/pers/hd";
	var lastInitial = lastName[0].toLowerCase();
	str += "/" + lastInitial + "/" + lastName + ":" + newName;
	return str;
};

function loadpersondetails(name, i) {
	$(".panel-body#" + i).html("Loading...");
	var string = "";
	var website = "";
	// console.log(homepages_result);
	for(var j = 0; j < homepages_result.length; j++) {
		if(homepages_result[j]['name'] == name) {
			website = homepages_result[j]['homepage'];
			break;
		}
	}
	string += "Personal website: <a href=\"" + website + "\" target=\"_blank\">link</a> ";
	
	function setHeader(xhr) {
		xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
	}
	$.ajax({
		url: "http://dblp.org/search/publ/api",
		type: "GET",
		data: {
			"q": name,
			"h": 1000,
			"format": "jsonp"
		},
		crossDomain: true,
		dataType: "jsonp",
		success: function(response) {
			if(response['result']['status']['@code'] == "200") {
				data = response['result']['hits'];
				// console.log(data);
				// alert(data['@total']);
				string += "DBLP profile: <a href=\"" + translateNameToDBLP(name) + "\" target=\"_blank\">link</a><br/>";
				string += "<hr/>";
				// string += data['@sent'] + " of " + data['@total'] + " articles:<br/>";
				hits = data['hit'];
				string += "<table id=\"" + name.replace(/ /g, "") + "\" class=\"table table-striped table-hover cell-border\"><thead><tr><th>Title</th><th>Type</th><th>Venue</th><th>Pages</th><th>Year</th><th>Link</th></tr></thead><tbody>";
				$.each(hits, function(index, value) {
					var info = value['info'];
					// console.log(info);
					if(info['pages'] == undefined)
						info['pages'] = "-";
					string += "<tr><td style=\"width: 40%; font-weight:bold;\">" + info['title'] + "</td><td>" + info['type'] + "</td><td style=\"width: 10%\">" + info['venue'] + "</td><td>" + info['pages'] + "</td><td>" + info['year'] + "</td><td><a href=\"" + info['url'] + "\" target=\"_blank\">link</a></td></tr>";
				});
				string += "</tbody></table>";
			}
			else {
				$(".panel-body#" + i).html("Error:" + response['result']['status']['text']);
			}
			$(".panel-body#" + i).html(string);
			$("#" + name.replace(/ /g, "") + "").DataTable();
			$("input[type='search']").each(function() {
				$(this).class()
			});
		},
		error: function(err) {
			alert("Error: " +err.responseText);
			$(".panel-body#" + i).html("Error!");
		},
		beforeSend: setHeader
	});

	// $.getJSON("http://dblp.org/search/publ/api", {"q": encodeURIComponent(name), "format": "json"}, function(data) {
	// 	console.log(data);
	// });

}

function showresults() {
	if(result.length == 0) {
		$(".result_persons").html("<h4>No result found.</h4><hr/>");
		return;
	}
	else {
		var string = "";
		string += 	"<h3 style=\"display:inline\">List of Persons</h3>"+
					"<span style=\"font-size: 12px; font-style: italic;\"> "+
					result.length +
					" result(s) found.</span><hr/>";
		string += "<table id=\"person_table\" class=\"table table-condensed borderless\"><thead><tr><th></th></tr></thead><tbody>";
		for(var i = 0; i < result.length; i++) {
			if(result[i] != undefined) {
				string +=
				"<tr><td>" + 
				"<div class=\"panel panel-default\">"+
					"<div class=\"panel-heading\">"+
						"<h4 class=\"panel-title\" style=\"display:inline\">"+
						"<a class=\"person_name\" onclick=\"loadpersondetails('" + result[i]['name'] + "', '" + i + "');\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + i + "\">"+
						result[i]['name']+
						"</a>"+
						"</h4>"+
						"<span style=\"font-size: 12px; font-style: italic;\"> "+
						result[i]['dept']+
						"</span>"+
					"</div>"+
					"<div id=\"collapse" + i + "\" class=\"panel-collapse collapse\">"+
						"<div class=\"panel-body\" id=\"" + i + "\" style=\"font-size: 14px\">"+
						"</div>"+
					"</div>"+
				"</div>" +
				"</td></tr>";
				// string += "<tr class=\"result_persons_tr\" name=\"" + result[i]['name'] + "\"><td>" + result[i]['name'] + "</td><td>" + result[i]['dept'] + "</td></tr>";
			}
		}
		string += "</tbody></table>"
		$(".result_persons").html(string);
		$("#person_table").DataTable({
			"pageLength": 50
		});
	}
}

function searchperson() {
	result = [];
	result_institution = [];
	names = [];
	if((window.person).length == 0)
		return;
	if(region.length == 0 || continent.length == 0) {
		$(".result_institutions").html("");
		$(".result_persons").html("");
		return;
	}
	var p = window.person;
	for(var i = 0; i < p.length; i++) {
		var isFoundInRegion = false;
		var isFoundInInstitution = false;
		for(var j = 0; j < region.length; j++) {
			if(p[i]['area'] == region[j]) {
				isFoundInRegion = true;
				break;
			}
		}
		for(var j = 0; j < institution.length; j++) {
			if(p[i]['dept'] == institution[j]) {
				isFoundInInstitution = true;
				break;
			}
		}
		if(isFoundInInstitution == true && isFoundInRegion == true) {

			if(names.indexOf(p[i]['name']) == -1) {
				result.push({'name': p[i]['name'], 'dept': p[i]['dept']});
				names.push(p[i]['name']);
			}
			if(result_institution.indexOf(p[i]['dept']) == -1)
				result_institution.push(p[i]['dept']);
		}
	}

	if(result_institution.length == 0) {
		$(".result_institutions").html("");
		if((window.person).length == 0)
			return;
		if(region.length == 0 || continent.length == 0)
			return;
	}
	else {
		var string = "";
		var numOfCol = 3;
		string += "<h3 style=\"display:inline\"> <input type=\"checkbox\" name=\"select_institution\" checked=\"checked\"/> List of Institutions</h3>" + 
		"<span style=\"font-size: 12px; font-style: italic;\"> "+
		result_institution.length +
		" result(s) found.</span><hr/>";

		var result_institution_matrix = listToMatrix(result_institution, numOfCol);
		string += "<table class=\"table table-bordered\">";
		for(var i = 0; i < result_institution_matrix.length; i++) {
			string += "<tr>";
			for(var j = 0; j < result_institution_matrix[i].length; j++) {
				string += "<td style=\"width: " + (100/numOfCol) + "%\"> <input type=\"checkbox\" name=\"result_institution_options\" value=\"" + result_institution_matrix[i][j] + "\" checked=\"checked\"/> " + result_institution_matrix[i][j] + "</td>";
			}
			string += "</tr>";
		}
		string += "</table>";
		$(".result_institutions").html(string);

		showresults();
	}
}

function searchpersonfrominstitutions() {
	result = [];
	names = [];
	var p = window.person;
	if(result_institution.length == 0) {
		$(".result_persons").html("");
		
	}
	else {
		var p = window.person;
		for(var i = 0; i < p.length; i++) {
			var isFoundInRegion = false;
			var isFoundInInstitution = false;
			for(var j = 0; j < region.length; j++) {
				if(p[i]['area'] == region[j]) {
					isFoundInRegion = true;
					break;
				}
			}
			for(var j = 0; j < result_institution.length; j++) {
				if(p[i]['dept'] == result_institution[j]) {
					isFoundInInstitution = true;
					break;
				}
			}
			if(isFoundInInstitution == true && isFoundInRegion == true) {
				if(names.indexOf(p[i]['name']) == -1) {
					result.push({'name': p[i]['name'], 'dept': p[i]['dept']});
					names.push(p[i]['name']);
				}
			}
		}
		showresults();
	}
}

$(document).ready(function() {
	$("input[type='checkbox'][name='continent'][value='asia']").prop({"checked": true});
	$("input[type='checkbox'][name='continent'][value='asia']").trigger("change");
});

$(document).on("change", "input[type='checkbox']", function() {
	var name = $(this).prop("name");
	var value = $(this).val();
	if(this.checked) {
		if(name == "region") {
			$(this).prop("checked", true);
			region.push(value);
		}
		else if(name == "continent") {
			$(this).prop("checked", true);
			continent.push(value);
		}
		else if(name == "select_region") {
			$("input[name='region']").each(function(i) {
				$(this).prop("checked", true);
			});
			region = [];
			for(var i = 0; i < region_result.length; i++) {
				region.push(region_result[i]['area_id']);
			}
		}
		else if(name == "select_continent") {
			$("input[name='continent']").each(function(i) {
				$(this).prop("checked", true);
			});
			continent = [];
			for(var i = 0; i < continent_result.length; i++) {
				continent.push(continent_result[i]['id']);
			}
		}
		else if(name == "select_institution") {
			result_institution = [];
			$("input[name='result_institution_options']").each(function(i) {
				$(this).prop("checked", true);
				result_institution.push($(this).val());
			});
			console.log(result_institution);
			searchpersonfrominstitutions();
			return;
		}
		else if(name == "result_institution_options") {
			result_institution.push(value);
			searchpersonfrominstitutions();
			return;
		}
	}
	else {
		if(name == "region") {
			$(this).prop("checked", false);
			region.remove(value);
		}
		else if(name == "continent"){
			$(this).prop("checked", false);
			continent.remove(value);
		}
		else if(name == "select_region") {
			$("input[name='region']").each(function(i) {
				$(this).prop("checked", false);
			});
			region = [];
		}
		else if(name == "select_continent") {
			$("input[name='continent']").each(function(i) {
				$(this).prop("checked", false);
			});
			continent = [];
		}
		else if(name == "select_institution") {
			result_institution = [];
			$("input[name='result_institution_options']").each(function(i) {
				$(this).prop("checked", false);
			});
			searchpersonfrominstitutions();
			return;

		}
		else if(name == "result_institution_options") {
			result_institution.remove(value);
			searchpersonfrominstitutions();
			return;
		}
	}

	institution = [];
	for(var i = 0; i < institution_result.length; i++) {
		if(continent.indexOf(institution_result[i]['region']) != -1) {
			institution.push(institution_result[i]['institution']);
		}
	}
	searchperson();
});