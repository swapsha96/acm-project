var region_url = "{{ url_for('static', filename='csv/area.csv') }}";
var region_result = {};
Papa.parse(region_url, {
	download: true,
	header: true,
	skipEmptyLines: true,
	complete: function(result) {
		region_result = result['data'];
		// console.log(region_result);
		$(".region").html("");
		for(var i = 0; i < region_result.length; i++) {
			if(region_result[i]['area_id'] != ""/* && region_result[i]['area_name'] != ""*/)
				$(".region").append("<li><input type=\"checkbox\" name=\"region\" value=\"" + region_result[i]['area_id'] + "\" > " + region_result[i]['area_name'] + " </li>");
		}
	}
});

var continent_result = [{'id':'asia','name': 'Asia'}, {'id':'europe','name': 'Europe'}, {'id':'australasia','name': 'Australia'}, {'id':'canada','name': 'Canada'}, {'id':'southamerica','name': 'South America'}, {'id':'america','name': 'North America'}];

var institution_url = "{{ url_for('static', filename='csv/country-info.csv') }}";
var institution_result = {};
Papa.parse(institution_url, {
	download: true,
	header: true,
	skipEmptyLines: true,
	complete: function(result) {
		institution_result = result['data'];
		// console.log(institution_result);
		for(var i = 0; i < continent_result.length; i++) {
			if(continent_result[i]['area_id'] != ""/* && region_result[i]['area_name'] != ""*/)
				$(".continent").append("<li><input type=\"checkbox\" name=\"continent\" value=\"" + continent_result[i]['id'] + "\" > " + continent_result[i]['name'] + " </li>");
		}
	}
});

var homepages_url = "{{ url_for('static', filename='csv/homepages.csv') }}";
var homepages_result = {};
Papa.parse(homepages_url, {
	download: true,
	header: true,
	skipEmptyLines: true,
	complete: function(result) {
		homepages_result = result['data'];
	}
});

var generatedAuthorInfo = "{{ url_for('static', filename='csv/generated-author-info.csv') }}";
var person = {};
Papa.parse(generatedAuthorInfo, {
	download: true,
	header: true,
	skipEmptyLines: true,
	complete: function(result) {
		person = result['data'];
		// console.log(person);
	}
});