import csv
import time

count = 0
names = []
persons = []
print("Loading file...")
then = time.time()
with open('generated-author-info.csv') as csvfile:
	reader = csv.DictReader(csvfile)
	for row in reader:
		if count % 1000 == 0:
			print("Processed " + str(count) + " entries...")
		count += 1
		name = row['name']
		if name not in names:
			names.append(name)
			persons.append(row)
		else:
			for person in persons:
				if person['name'] == name:
					person['count'] = str(float(person['count']) + float(row['count']))
					person['adjustedcount'] = str(float(person['adjustedcount']) + float(row['adjustedcount']))
					break
# for person in persons:
# 	person['adjustedcount'] = round()
print("Process " + str(len(persons)) + " reduced entries.")
print("Writing reduced data...")
with open('reduced-generated-author-info.csv', 'w') as csvfile:
	fieldnames = ['name', 'dept', 'area', 'count', 'adjustedcount']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()
	for person in persons:
		writer.writerow({'name': person['name'], 'dept': person['dept'], 'area': person['area'], 'count': person['count'], 'adjustedcount': person['adjustedcount']})
now = time.time()
print("Stored in reduced-generated-author-info.csv")
print("time taken: " + str(now-then) + " ")